import { useState } from "react";
import "./App.css";
import Login from "./components/Login";
import Navbar from "./components/Navbar";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import PrivateUser from "./private-router/Privateuser";
import User from "./views/User";
import DetailUser from "./views/DetailUser";

function App() {
  const [token, setToken] = useState(localStorage.getItem("userToken") ?? null);
  return (
    <div className="App">
      <Navbar token={token} setToken={setToken} />
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={<Login token={token} setToken={setToken} />}
          />
          <Route
            path="/user"
            element={
              <PrivateUser>
                <User />
              </PrivateUser>
            }
          />
          <Route
            path="/user/detail/:id"
            element={
              <PrivateUser>
                <DetailUser />
              </PrivateUser>
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
