import axios from "axios";
import React, { useEffect, useState } from "react";

function User() {
  const [user, setUser] = useState([]);
  useEffect(() => {
    const getUserAll = async () => {
      await axios
        .get("https://fakestoreapi.com/users")
        .then((res) => {
          setUser(res.data);
          console.log(res.data);
        })
        .catch((error) => {
          console.log(error);
        });
    };
    getUserAll();
  }, []);

  return (
    <>
      <div className="container mt-5">
        <div className="card p-3">
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th>No</th>
                <th>Full Name</th>
                <th>Username</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>
              {user.map((users, i) => (
                <tr>
                  <th>{i + 1}</th>
                  <td>
                    {users.name.firstname} {users.name.lastname}
                  </td>
                  <td>{users.username}</td>
                  <td>{users.email}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}

export default User;
