import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

function DetailUser() {
  const { id } = useParams();
  const [user, setUser] = useState({
    name: {},
  });
  useEffect(() => {
    const userDetail = async () => {
      await axios
        .get(`https://fakestoreapi.com/users/${id}`)
        .then((res) => {
          setUser(res.data);
          console.log(res.data);
        })
        .catch((error) => {
          console.log(error);
        });
    };
    userDetail();
  }, []);

  return (
    <>
      <div className="container mt-5">
        <div className="card p-3 row">
          <div className="col-6"></div>
          <div className="col-6"></div>
        </div>
      </div>
    </>
  );
}

export default DetailUser;
