import React from "react";
import "../App.css";
import Swal from "sweetalert2";

const Navbar = ({ token, setToken }) => {
  const logoutHandler = () => {
    Swal.fire({
      position: "center",
      icon: "success",
      title: "Logout Success!!",
      showConfirmButton: false,
      timer: 1500,
    });
    setToken("");
    localStorage.clear();
  };
  return (
    <>
      <div className="navbar">
        <h1>Merkle Innovation Test</h1>

        {token ? (
          <button className="log-out-btn" onClick={() => logoutHandler()}>
            Log Out
          </button>
        ) : (
          ""
        )}
      </div>
    </>
  );
};

export default Navbar;
