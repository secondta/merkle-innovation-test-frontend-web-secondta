import React, { useState } from "react";
import "../App.css";
import axios from "axios";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

const Login = ({ token, setToken }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const navigate = useNavigate();

  const loginHandler = () => {
    setError("");
    setPassword("");
    setUsername("");
    axios({
      url: "https://fakestoreapi.com/auth/login",
      method: "POST",
      data: {
        username: username,
        password: password,
      },
    })
      .then((res) => {
        Swal.fire({
          position: "center",
          icon: "success",
          title: "Login Success!!",
          showConfirmButton: false,
          timer: 1500,
        });
        console.log(res.data.token);
        setToken(res.data.token);
        localStorage.setItem("userToken", "testing");
        navigate("/user");
      })
      .catch((err) => {
        Swal.fire({
          position: "center",
          icon: "error",
          title: "Login gagal!!",
          showConfirmButton: false,
          timer: 1500,
        });
        console.log(err.response);
        setError(err.response.data);
      });
  };

  return (
    <>
      <div className="login">
        <div className="login-inputs">
          <h2>Login Form</h2>
          <input
            className="input"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            type="text"
            placeholder="Username"
          />
          <input
            className="input"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            type="password"
            placeholder="Password"
          />
          {error && <small>{error}</small>}
          <button type="button" onClick={loginHandler}>
            Login
          </button>
        </div>
      </div>
    </>
  );
};

export default Login;
